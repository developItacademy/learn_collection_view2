//
//  AssetExtractor.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 9/9/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit
import UserNotifications
import CloudKit

enum GetStringFromError: Error {
    case delimeterNotFound
    case notEnoughProfiles
}


struct HelperFunctions {
    
        
        static func getStringFrom(passedString: String, using delimiter: String = "~#~") throws -> (extractedString: String, cleanString: String) {
            
            guard let startIdx = passedString.range( of: delimiter,
                                                options: NSString.CompareOptions.literal,
                                                range: passedString.startIndex..<passedString.endIndex,
                                                locale: nil)?.upperBound
                                                        else {
                                                            print("error on start index")
                                                            throw GetStringFromError.delimeterNotFound}
            
            guard let endIdx = passedString.range( of: delimiter,
                                                options: NSString.CompareOptions.literal,
                                                range: startIdx..<passedString.endIndex,
                                                locale: nil)?.lowerBound
                                                      else { print("error on end index")
                                                        throw GetStringFromError.delimeterNotFound}

            
            let returnedSubstr = passedString[startIdx..<endIdx]
            
            let testingNumberOccurs = String(returnedSubstr).components(separatedBy:";")
            guard  testingNumberOccurs.count == 5 else {print("on number of occurences")
                                                        throw GetStringFromError.notEnoughProfiles}
            
            
            let cleanStr = passedString.replacingOccurrences(of: delimiter + returnedSubstr + delimiter, with: "")
             
            return ( String(returnedSubstr), String(cleanStr) )

        }
        
    
    struct AssetExtractor {
        
        static func createLocalUrl(forImageNamed name: String) -> URL? {
            
            let fileManager = FileManager.default
            let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
            let url = cacheDirectory.appendingPathComponent("\(name).jpeg")
            let path = url.path
            
            guard fileManager.fileExists(atPath: path) else {
                guard
                    let image = UIImage(named: name),
                    let data = image.jpegData(compressionQuality: 100.0)
                    else { return nil }
                
                fileManager.createFile(atPath: path, contents: data, attributes: nil)
                return url
            }
            
            return url
        }
        
    }
    
    struct NotificationsHelper {
        
        static func sendNotification(for theNextStudent: User) {
            
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                let authorizationStatus = settings.authorizationStatus
                switch authorizationStatus {
                    case .authorized: print("```authorized")
                    case .denied: print("```denied")
                    case .notDetermined: print("```not determined")
                    case .provisional: print("```provisional")
                    @unknown default: break
                }
            }
            
            let request = createNotificationRequest(for: theNextStudent)
            UNUserNotificationCenter.current().add(request) { (error) in
                if error == nil {
                    let operation = OperationQueue.main
                    operation.addOperation {
                        //self.msgToSend?.text = ""
                    }
                }

            }
//            UNUserNotificationCenter.current().add(request) { (error) in
//                if error == nil {
//                    let operation = OperationQueue.main
//                    operation.addOperation {
//                        //self.msgToSend?.text = ""
//                    }
//                }
//            }
            
        }
        
        private static func createNotificationRequest(for theNextStudent: User) -> UNNotificationRequest {
            
            /// Create the notification content
            let notificationContent: UNMutableNotificationContent = {
                let content = UNMutableNotificationContent()
                content.title = "Next Student To Login"
                content.body = "Touch to activte it"
                content.sound = UNNotificationSound.defaultCriticalSound(withAudioVolume: 1.0)
                print("```* * * before if ")
                
                
                
                print("```* * * after first if ", theNextStudent.firstName)
                if let picURL = HelperFunctions.AssetExtractor.createLocalUrl(forImageNamed: theNextStudent.username), let attach = try? UNNotificationAttachment(identifier: "studentPic", url: picURL, options: nil)  {
                    print("```* * * after second if ", theNextStudent.username, picURL)
                        content.attachments = [attach]
                    }
                let encoder = JSONEncoder()
                guard let theNextStudentEnconded = try? encoder.encode(theNextStudent) else {fatalError("Error - could not encode student")}
                content.userInfo = ["name" : theNextStudentEnconded]
                
                return content
            }()
            
            /// Create Category and Action - from Simon NG
            let categoryIdentifer = "switchStudent"
            
            let cancelAction = UNNotificationAction(identifier: "switchStudent.cancel", title: "Cancel", options: [])
            
            let category = UNNotificationCategory(identifier: categoryIdentifer, actions: [cancelAction], intentIdentifiers: [], options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([category])
            
            notificationContent.categoryIdentifier = categoryIdentifer
            
            /// Create the notification trigger
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1200, repeats: false)
            
            /// create the request
            return UNNotificationRequest(identifier: "firstNotification", content: notificationContent, trigger: trigger)
        }
    }
    
    struct GeneralInfo {
        
        static var dbs : CKDatabase {
            return CKContainer(identifier: "iCloud.com.dia.cloudKitExample.open").publicCloudDatabase
        }

        
        static var iPadID: String = {
            
            /// get the uuid identifier
            guard let uuid = UIDevice.current.identifierForVendor?.uuidString else { fatalError("Error - can not convert UUID String") }
            
            var iPadID: String = ""
            
            let id = CKRecord.ID(recordName: uuid)
            
            dbs.fetch(withRecordID: id) { (record, error) in
                guard error == nil, let record = record  else { fatalError("Error reading the iPad record \(error?.localizedDescription)") }

                iPadID = record["identifier"]!
                print("~~~", iPadID, "---", uuid)
            }
            
            return iPadID

        }()
    }

}
