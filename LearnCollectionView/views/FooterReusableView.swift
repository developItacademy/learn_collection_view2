//
//  FooterReusableView.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit

class FooterReusableView: UICollectionReusableView {
        
    @IBOutlet weak var image: UIImageView!
}
